import java.util.*;

public class MyArrayList<E> implements List<E>, RandomAccess {
    @SuppressWarnings("unchecked")
    private E[] tab = (E[]) new Object[1];
    private int size = 0;

    // Garantit que la place disponible est au minimum minCapacity
    private void ensureCapacity(int minCapacity) {
        if (tab.length < minCapacity) {
            grow(minCapacity);
        }
    }

    // Re-alloue tab pour qu’il soit au minimum une fois et demi plus grand et de la capacite demandee
    private void grow(int minCapacity) {
        int newCapacity = (3 * tab.length + 1) / 2;
        if (newCapacity < minCapacity)
            newCapacity = minCapacity;
        @SuppressWarnings("unchecked")
        E[] newTab = (E[]) new Object[newCapacity];
        for (int i = 0; i < tab.length; ++i) {
            newTab[i] = tab[i];
        }
        tab = newTab;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyArrayListIterator<>(tab);
    }

    @Override
    public boolean isEmpty() {
        return this.size==0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean add(E e) {
        ++this.size;
        ensureCapacity(this.size);
        this.tab[this.size-1]= e;
        return true;
    }

    @Override
    public void add(int index, E element) {
        if(isIndexNotOutOfBound(index)){
            ++this.size;
            ensureCapacity(this.size);
            for(int i = this.size-1; i>index; --i){
                tab[i] = tab[i-1];
            }
            tab[index]=element;
        }
    }

    @Override
    public E get(int index) {
        if(isIndexNotOutOfBound(index)) {
            return tab[index];
        }
        return null;
    }

    public boolean isIndexNotOutOfBound(int index){
        if(index>=this.size){
            throw new IndexOutOfBoundsException("Index " + index + " out of bounds.");
        }
        return true;
    }

    @Override
    public E set(int index, E element) {
        if(isIndexNotOutOfBound(index)){
            E prec = this.tab[index];
            this.tab[index] = element;
            return prec;
        }
        return null;
    }

    @Override
    public E remove(int index) {
        if(isIndexNotOutOfBound(index)) {
            E elem = this.tab[index];
            -- this.size;
            for(int i = index; i<this.size; ++i){
                tab[i] = tab[i+1];
            }
            tab[this.size] =null;
            return elem;
        }
        return null;
    }

    @Override
    public boolean remove(Object o) {
        int posO = indexOf(o);
        if(posO != -1) {
            remove(posO);
            return true;
        }
        return false;
    }

    @Override
    public void clear() {
        this.tab = (E[]) new Object[1];
        this.size = 0;
    }

    @Override
    public int indexOf(Object o) {
        int index =0;
        int posO = -1;
        while(index < this.tab.length && posO == -1){
            if(this.tab[index].equals(o)){
                posO = index;
            }
            ++index;
        }
        return posO;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index =this.tab.length-1;
        int posO = -1;
        while(index >= 0 && posO == -1){
            if(this.tab[index].equals(o)){
                posO = index;
            }
            --index;
        }
        return posO;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public boolean equals(Object o){
        boolean isEq = false;
        if (o instanceof List) {
            List<E> it = (List<E>)o;
            if(it.size() == this.size()){
                isEq = true;
                for(int i = 0; i< this.size; ++i){
                    if(!this.get(i).equals(it.get(i))){
                        isEq = false;
                    }
                }
            }
        }
        return isEq;
    }

    //méthodes pas complétées:
    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }

}

class MyArrayListIterator<E> implements Iterator<E>{
    private E[] tab = (E[]) new Object[1];
    private int indice = 0;

    MyArrayListIterator(E[] tab){
        this.tab = tab;
    }

    @Override
    public boolean hasNext() {
        return this.tab.length > indice && tab[indice] != null;
    }

    @Override
    public E next() {
        if(!hasNext())
            throw new NoSuchElementException();
        ++this.indice;
        return this.tab[indice-1];
    }

}