
import java.util.ArrayList;
import java.util.List;
import java.util.RandomAccess;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class MyArrayListTest {

    @Test
    public void testAddSizeIsEmpty(){
        List<String> li = new MyArrayList<>();
        assertTrue(li.isEmpty());
        assertTrue(li.size()==0);
        assertTrue(li.add("A"));
        assertFalse(li.isEmpty());
        assertTrue(li.size()==1);
        assertTrue(li.add("B"));
        assertFalse(li.isEmpty());
        assertTrue(li.size()==2);
        assertTrue(li.add("C"));
        assertFalse(li.isEmpty());
        assertTrue(li.size()==3);
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testAddToEndAndGet(){
        List<String> li = new MyArrayList<>();
        assertTrue(li.add("A"));
        assertTrue(li.add("B"));
        assertTrue(li.add("C"));
        assertEquals("A",li.get(0));
        assertEquals("B",li.get(1));
        assertEquals("C",li.get(2));
        li.get(3);              //IndexOutOfBoundsException
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testAddToEndAndSet(){
        List<String> li = new MyArrayList<>();
        assertTrue(li.add("A"));
        assertTrue(li.add("B"));
        assertTrue(li.add("C")); //A B C
        assertEquals("A",li.set(0,"X"));
        assertEquals("X",li.get(0)); //X B C
        assertEquals("B",li.set(1,"Y"));
        assertEquals("Y",li.get(1)); //X Y C
        assertEquals("C",li.set(2,"Z"));
        assertEquals("Z",li.get(2)); //X Y Z
        li.set(3,"W");              //IndexOutOfBoundsException
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testAddIndex(){
        List<String> li = new MyArrayList<>();
        assertTrue(li.add("A"));
        assertTrue(li.add("B"));
        assertTrue(li.add("C"));
        li.add(0,"X");              //X A B C
        assertEquals("X",li.get(0));
        assertEquals("A",li.get(1));
        assertEquals("B",li.get(2));
        assertEquals("C",li.get(3));
        li.add(2,"Z");              //X A Z B C
        assertEquals("X",li.get(0));
        assertEquals("A",li.get(1));
        assertEquals("Z",li.get(2));
        assertEquals("B",li.get(3));
        assertEquals("C",li.get(4));
        li.add(6,"Y");              //IndexOutOfBoundsException
    }

    @Test(expected=IndexOutOfBoundsException.class)
    public void testRemove(){
        List<String> li = new MyArrayList<>();
        assertTrue(li.add("A"));
        assertTrue(li.add("B"));
        assertTrue(li.add("C"));
        assertTrue(li.add("D"));
        assertTrue(li.add("E"));
        assertEquals("C",li.remove(2)); //A B D E
        assertTrue(li.size()== 4);
        assertEquals("A",li.get(0));
        assertEquals("B",li.get(1));
        assertEquals("D",li.get(2));
        assertEquals("E",li.get(3));

        assertEquals("A",li.remove(0)); //B D E
        assertTrue(li.size()== 3);
        assertEquals("B",li.get(0));
        assertEquals("D",li.get(1));
        assertEquals("E",li.get(2));

        assertEquals("E",li.remove(2)); //B D
        assertTrue(li.size()== 2);
        assertEquals("B",li.get(0));
        assertEquals("D",li.get(1));

        li.remove(5); //out of bounds
    }

    @Test
    public void testIterator(){
        List<String> li = new MyArrayList<>();
        li.add("A");
        li.add("B");
        li.add("C");
        String[] tab = new String[3];
        int i = 0;
        for(String s: li)
            tab[i++] = s;
        assertEquals("A",tab[0]);
        assertEquals("B",tab[1]);
        assertEquals("C",tab[2]);


    }

    @Test
    public void testRandomAccess() {
        assertTrue(new MyArrayList<Integer>() instanceof RandomAccess);
    }

    @Test
    public void testIndexOf(){
        List<String> li = new MyArrayList<>();
        li.add("A");
        li.add("B");
        li.add("A");
        assertEquals(0,li.indexOf("A"));
        assertEquals(1,li.indexOf("B"));
        assertEquals(-1,li.indexOf("C"));
    }

    @Test
    public void testLastIndexOf(){
        List<String> li = new MyArrayList<>();
        li.add("A");
        li.add("B");
        li.add("A");
        assertEquals(2,li.lastIndexOf("A"));
        assertEquals(1,li.lastIndexOf("B"));
        assertEquals(-1,li.lastIndexOf("C"));
    }

    @Test
    public void testContains(){
        List<String> li = new MyArrayList<>();
        li.add("A");
        li.add("B");
        assertTrue(li.contains("A"));
        assertTrue(li.contains("B"));
        assertFalse(li.contains("C"));
    }

    @Test
    public void testClear(){
        List<String> li = new MyArrayList<>();
        li.add("A");
        li.add("B");
        assertTrue(li.size()==2);
        li.clear();
        assertTrue(li.size()==0);
        assertTrue(li.isEmpty());
    }

    @Test
    public void testEquals(){
        List<String> l1 = new MyArrayList<>();
        List<String> l2 = new MyArrayList<>();
        assertEquals(l2, l1);
        l1.add("A");
        l2.add("A");
        l1.add("B");
        l2.add("B");
        assertEquals(l2, l1);
        l2.add("C");
        assertFalse(l1.equals(l2));
        List<Integer> l3 = new MyArrayList<>();
        l3.add(1);
        l3.add(2);
        assertFalse(l1.equals(l3));

        List<String> l4 = new MyLinkedList2<String>();
        l4.add("A");
        l4.add("B");
        assertTrue(l1.equals(l4));
        //l'inverse n'est pas vrai car l'implémentation java.util se base
        //sur des ListIterator que nous l'avons pas implémenté.
        //assertTrue(l4.equals(l1))
    }

}
