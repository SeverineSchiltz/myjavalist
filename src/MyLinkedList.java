import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyLinkedList<E> implements MyList<E> {
    private Node<E> head = null; //liste vide par défaut
    private int size=0;

    private Node<E> getNode(int index) {
        if (index >= 0 && index < this.size()) {
            Node<E> current = head;
            for (int i = 0; i < index; ++i) {
                current = current.next;
            }
            return current;
        }
        throw new IndexOutOfBoundsException("Index " + index + " out of bounds.");
    }

    @Override
    public boolean add(E e) {
        this.add(this.size, e);
        return true;
    }

    @Override
    public void add(int index, E e) {
        if (index == 0) {
            this.head = new Node<>(e, head);
        } else {
            Node<E> prec = getNode(index - 1);
            prec.next = new Node<>(e, prec.next);
        }
        ++size;
    }

    @Override
    public E get(int index) {
        return getNode(index).elem;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public E remove(int index) {
        //ajouter le cas de la tête (si c'est le dernier élément faut le mettre à null)
        if (this.isEmpty()) {
            return null;
        } else {
            E elemPrec = getNode(index).elem;
            getNode(index-1).next = getNode(index+1);
            return elemPrec;
//il y a plus performant pour pas appeler 2x getNode (voir solution prof):
//            Node<E> prec = getNode(index - 1);
//            old = prec.next.elem;
//            prec.next = prec.next.next;
        }
    }

    @Override
    public E set(int index, E e) {
        if (this.isEmpty()) {
            this.head = new Node<>(e, head);
            return null;
        } else {
            E elemPrec = getNode(index).elem;
            getNode(index).elem = e;
            return elemPrec;
        }
    }


    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyLinkedListIterator<>(head);
    }
}

class Node<E> {
    //remarque, encapsulation non nécessaire, on "cache" cette classe
//dans le package : elle n’est pas accessible pour les utilisateurs
    E elem;
    Node<E> next;
    Node(E elem, Node<E> next) {
        this.elem = elem;
        this.next = next;
    }
}

/* Attention, passer à l'élément suivant ne doit coûter qu'un seul "saut"
   et pas tout un parcours.
*/
class MyLinkedListIterator<E> implements Iterator<E>{
    private Node<E> current;

    //constructeur: on lui passe le head:
    MyLinkedListIterator(Node<E> start){
        current = start;
    }

    @Override
    public boolean hasNext() {
        return current != null;
    }

    @Override
    public E next() {
        if(current == null)
            throw new NoSuchElementException();
        E e = current.elem;
        current = current.next;
        return e;
    }

}