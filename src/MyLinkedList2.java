import java.util.*;

public class MyLinkedList2<E> implements List<E> {
    private Node<E> head = null; //liste vide par défaut
    private int size=0;

    private Node<E> getNode(int index) {
        if (index >= 0 && index < this.size()) {
            Node<E> current = head;
            for (int i = 0; i < index; ++i) {
                current = current.next;
            }
            return current;
        }
        throw new IndexOutOfBoundsException("Index " + index + " out of bounds.");
    }

    @Override
    public boolean add(E e) {
        this.add(this.size, e);
        return true;
    }

    @Override
    public void clear() {
        this.head = null;
        this.size = 0;
    }

    @Override
    public void add(int index, E e) {
        if (index == 0) {
            this.head = new Node<>(e, head);
        } else {
            Node<E> prec = getNode(index - 1);
            prec.next = new Node<>(e, prec.next);
        }
        ++size;
    }

    @Override
    public E get(int index) {
        return getNode(index).elem;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public E remove(int index) {
        //à refaire et revoir solution prof!
        if (this.isEmpty()) {
            return null;
        } else {
            E elemPrec = getNode(index).elem;
            getNode(index-1).next = getNode(index+1);
            return elemPrec;
            //il y a plus performant pour pas appeler 3x getNode (voir solution prof):
//            Node<E> prec = getNode(index - 1);
//            E old = prec.next.elem;
//            prec.next = prec.next.next;
//            --size;
//            return old;
        }
        //il faut aussi prévoir le cas du premier élément (head) qui n'a pas de précédent!
    }

    @Override
    public boolean remove(Object o) {
        //a faire
        return false;
    }

    @Override
    public int indexOf(Object o) {
            Node<E> current = head;
            for (int index = 0; index < this.size(); ++index) {
                if(current.elem.equals(o)){
                    return index;
                }
                current = current.next;
            }
        return -1;
    }

    //plus épuré que indexOf:
    @Override
    public int lastIndexOf(Object o) {
        int pos=-1, index =0;
        for(E e : this){
            if(e.equals(o)){
                pos = index;
            }
            ++index;
        }
        return pos;
    }

    @Override
    public E set(int index, E e) {
        if (this.isEmpty()) {
            this.head = new Node<>(e, head);
            return null;
        } else {
            E elemPrec = getNode(index).elem;
            getNode(index).elem = e;
            return elemPrec;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<E> iterator() {
        return new MyLinkedListIterator<>(head);
    }

    @Override
    public boolean equals(Object o) {
        boolean isEq = true;
        if (o instanceof List) {
            List<E> that = (List<E>) o;
            if(this.size != that.size()){
                isEq = false;
            }
            else{
                Iterator<E> io = that.iterator();
                for (E e : this) {
                    if (!e.equals(io.next())) {
                        isEq = false;
                        break;
                    }
                }
            }
        }
        return isEq;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }



    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }
}

