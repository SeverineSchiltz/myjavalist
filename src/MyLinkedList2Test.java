import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MyLinkedList2Test {

    @Test
    public void testIndexOf(){
        List<String> li = new MyLinkedList2<>();
        li.add("A");
        li.add("B");
        li.add("A");
        assertEquals(0,li.indexOf("A"));
        assertEquals(1,li.indexOf("B"));
        assertEquals(-1,li.indexOf("C"));
    }

    @Test
    public void testLastIndexOf(){
        List<String> li = new MyLinkedList2<>();
        li.add("A");
        li.add("B");
        li.add("A");
        assertEquals(2,li.lastIndexOf("A"));
        assertEquals(1,li.lastIndexOf("B"));
        assertEquals(-1,li.lastIndexOf("C"));
    }

    @Test
    public void testContains(){
        List<String> li = new MyLinkedList2<>();
        li.add("A");
        li.add("B");
        assertTrue(li.contains("A"));
        assertTrue(li.contains("B"));
        assertFalse(li.contains("C"));
    }

    @Test
    public void testClear(){
        List<String> li = new MyLinkedList2<>();
        li.add("A");
        li.add("B");
        assertTrue(li.size()==2);
        li.clear();
        assertTrue(li.size()==0);
        assertTrue(li.isEmpty());
    }

    @Test
    public void testRemove(){
        List<String> li = new MyLinkedList2<>();
        li.add("A");
        li.add("B");
        li.add("A");
        assertTrue(li.remove("A"));
        assertTrue(li.size()==2);
        assertEquals("B",li.get(0));
        assertEquals("A",li.get(1));

        assertFalse(li.remove("C"));

        assertTrue(li.remove("A"));
        assertTrue(li.size()==1);
        assertEquals("B",li.get(0));

        assertFalse(li.remove("A"));

        assertTrue(li.remove("B"));
        assertTrue(li.size()==0);
        assertTrue(li.isEmpty());
    }

    @Test
    public void testEquals(){
        List<String> l1 = new MyLinkedList2<>();
        List<String> l2 = new MyLinkedList2<>();
        assertEquals(l2, l1);
        l1.add("A");
        l2.add("A");
        l1.add("B");
        l2.add("B");
        assertEquals(l2, l1);
        l2.add("C");
        assertFalse(l1.equals(l2));
        List<Integer> l3 = new MyLinkedList2<>();
        l3.add(1);
        l3.add(2);
        assertFalse(l1.equals(l3));

        List<String> l4 = new ArrayList<String>();
        l4.add("A");
        l4.add("B");
        assertTrue(l1.equals(l4));
        //l'inverse n'est pas vrai car l'implémentation java.util se base
        //sur des ListIterator que nous l'avons pas implémenté.
        //assertTrue(l4.equals(l1))
    }
}