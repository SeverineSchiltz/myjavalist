import org.junit.Test;

import static org.junit.Assert.*;

public class MyLinkedListTest {

    @Test
    public void add() {
        MyLinkedList<Integer> myList = new MyLinkedList<Integer>();
        myList.add(2);
        myList.add(4);
        myList.add(5);
        assertEquals(new Integer(2), myList.get(0));
        assertEquals(new Integer(4), myList.get(1));
        assertEquals(new Integer(5), myList.get(2));
    }

    @org.junit.Test
    public void get() {
        MyLinkedList<Integer> myList = new MyLinkedList<Integer>();
        myList.add(0, 2);
        myList.add(1,4);
        myList.add(2,5);
        assertEquals(new Integer(2), myList.get(0));
        assertEquals(new Integer(4), myList.get(1));
        assertEquals(new Integer(5), myList.get(2));
    }

    @org.junit.Test
    public void isEmpty() {
    }

    @org.junit.Test
    public void remove() {
    }

    @org.junit.Test
    public void set() {

    }

    @org.junit.Test
    public void size() {
    }
}